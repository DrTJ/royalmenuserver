﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoyalMenuServer.Models.Helpers
{
    public class GeoPoint
    {
        public int Latitude { get; set; }
        public int Longitude { get; set; }
    }
}
