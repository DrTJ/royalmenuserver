﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoyalMenuServer.Models.Helpers
{
    public interface ICrud<T>
    {
        void Add(T newItem);
        void Delete(T itemToDelete);
        void Update(T itemToUpdate);

        IEnumerable<T> GetAll();
        IEnumerable<T> GetAllDeeply();
    }
}
