﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RoyalMenuServer.Models.Helpers;

namespace RoyalMenuServer.Models.Repositories
{
    public class RestaurantsRepository : RepositoryBase, ICrud<Restaurants>
    {
        #region Get Items

        public IEnumerable<Restaurants> GetAll()
        {
            var result = Context.Restaurants;
            return result;
        }

        public IEnumerable<Restaurants> GetAllDeeply()
        {
            var result = Context.Restaurants.Include(x => x.RestaurantMenu)
                                            .Include(menu => menu.RestaurantMenu.Select(item => item.MenuItemPictures))
                                            .Include(menu => menu.RestaurantMenu.Select(item => item.FoodTags));
            return result;
        }
        
        public Restaurants GetRestaurant(long id)
        {
            var result = Context.Restaurants.Include(x => x.RestaurantMenu)
                                            .Include(menu => menu.RestaurantMenu.Select(item => item.MenuItemPictures))
                                            .Include(menu => menu.RestaurantMenu.Select(item => item.FoodTags))
                                            .FirstOrDefault(item => item.IdRestaurant == id);
            return result;
        }

        public IEnumerable<Restaurants> GetRestaurantByDistance(GeoPoint geoPoint, int radius, long cityId)
        {
            var result = Context.Restaurants.Include(x => x.RestaurantMenu)
                                            .Include(menu => menu.RestaurantMenu.Select(item => item.MenuItemPictures))
                                            .Include(menu => menu.RestaurantMenu.Select(item => item.FoodTags))
                                            .Where(item => item.CityId == cityId && IsInRange(geoPoint, radius, item.Latitude, item.Longitude));
            return result;
        }

        #endregion

        public void Add(Restaurants newItem)
        {
            newItem.IdRestaurant = GetNewId();

            Context.Restaurants.Add(newItem);
            Context.SaveChanges();
        }

        public void Delete(Restaurants itemToDelete)
        {
            var item = GetRestaurant(itemToDelete.IdRestaurant);
            if(item == null)
            {
                return;
            }

            Context.Restaurants.Remove(item);
            
            // ToDo: Delete the depended Pictures, Videos, etc... from the disc.

            Context.SaveChanges();
        }

        public void Update(Restaurants itemToUpdate)
        {
            throw new NotImplementedException();
        }

        #region Internal methods

        public long GetNewId() => Context.Restaurants?.Max(w => w.IdRestaurant) ?? 0;

        private bool IsInRange(GeoPoint geoPoint, int radius, string latitude, string longitude)
        {
            return true;
        }

        #endregion
    }
}
