﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoyalMenuServer.Models.Repositories
{
    public class RepositoryBase
    {
        public RoyalMenuDataEntities Context { get; set; }

        public RepositoryBase()
        {
            Context = new RoyalMenuDataEntities();
            Context.Configuration.ProxyCreationEnabled = false;
        }
    }
}
