﻿using RoyalMenuServer.Models;
using RoyalMenuServer.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Royal_Menu_Server.Controllers
{
    public class RestaurantsController : ApiController
    {
        #region Fields

        private RestaurantsRepository restaurantsRepository;

        #endregion

        #region Constructors

        public RestaurantsController()
        {
            restaurantsRepository = new RestaurantsRepository();
        }

        #endregion
        
        public IEnumerable<Restaurants> Get()
        {
            return restaurantsRepository.GetAllDeeply();
        }
    }
}
